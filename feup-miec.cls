\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{feup-miec}[2015/07/18 v1.0 feup civil master degree thesis]

% BASE CLASS - BOOK
\LoadClass[11pt,twoside]{book} %import book class
\setcounter{secnumdepth}{3} %set subsubsection numbering on
\setcounter{tocdepth}{3}
\setlength{\parskip}{6pt plus0pt minus0pt} %set paragraph skip
\setlength{\parindent}{0pt} % no indent
\sloppy %force no text in margins

%PACKAGES
\RequirePackage[portuguese]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{geometry} %set page layout, margins etc
\RequirePackage{fancyhdr} %costumizable header and footer
\RequirePackage{mathptmx} %Times New Roman font
\RequirePackage{titlesec} %costumizable titles
\RequirePackage[scaled=.90]{helvet} %Arial font with command \sffamily
\RequirePackage{etoolbox}
\RequirePackage{tocloft} %costumize toc
\RequirePackage{color} %enable colors to text
\RequirePackage{graphicx} %enable figures
\RequirePackage[table,xcdraw,dvipsnames]{xcolor} %enable color fill in tables
\RequirePackage{float} %to use [H] to fix tables and figures position
\RequirePackage{caption} %to change two dots by hyphen in captions
\RequirePackage{listings} %allow script snippets
\RequirePackage{multirow} %enable multirows on tables
\RequirePackage{pdfpages} %allow includepdf
\RequirePackage{lipsum}
%\RequirePackage{enumitem}
%\RequirePackage{jurabib}
%\RequirePackage{hyperref}
%\RequirePackage[dvipsnames]{xcolor} % MAY CAUSE CONFLICTS WITH COLOR PACKAGE - TEST

% script snippets config
\definecolor{c_01}{rgb}{0,0.6,0}
\definecolor{c_02}{rgb}{0.5,0.5,0.5}
\definecolor{c_03}{rgb}{0.58,0,0.82}
\definecolor{light-gray}{gray}{0.95}
\renewcommand{\lstlistingname}{\sffamily\fontsize{9}{14}\selectfont Teste}
\lstset{
	backgroundcolor=\color{light-gray},  % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
 	basicstyle=\footnotesize,        	 % the size of the fonts that are used for the code
 	breakatwhitespace=false,         	 % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 	 % sets automatic line breaking
  	captionpos=b,                    	 % sets the caption-position to bottom
  	commentstyle=\color{gray},    	 	 % comment style
  	%deletekeywords={...},            	 % if you want to delete keywords from the given language
  	escapeinside={\%*}{*)},          	 % if you want to add LaTeX within your code
  	extendedchars=true,                  % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  	frame=single,	                 	 % adds a frame around the code
  	keepspaces=true,                     % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  	keywordstyle=\color{blue},           % keyword style
  	language=Python,                     % the language of the code
  	%otherkeywords={*,...},               % if you want to add more keywords to the set
  	numbers=left,                        % where to put the line-numbers; possible values are (none, left, right)
  	numbersep=5pt,                       % how far the line-numbers are from the code
  	numberstyle=\tiny\color{c_02},       % the style that is used for the line-numbers
  	rulecolor=\color{black},             % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  	showspaces=false,                    % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  	showstringspaces=false,              % underline spaces within strings only
  	showtabs=false,                      % show tabs within strings adding particular underscores
  	stepnumber=1,                        % the step between two line-numbers. If it's 1, each line will be numbered
  	stringstyle=\color{BrickRed},        % string literal style
  	tabsize=4,	                         % sets default tabsize to 2 spaces
  	%title=\lstname
}
% \script snippets config


\DeclareCaptionLabelSeparator{hyphen}{ - }
\captionsetup{
  labelsep=hyphen
}

\graphicspath{{./Chapters/Images/}}
\addto\captionsportuguese{
	\renewcommand{\figurename}{\sffamily\fontsize{9}{14}\selectfont Fig.}
	\renewcommand{\tablename}{\sffamily\fontsize{9}{14}\selectfont Quadro} 
} % change figure caption word and style
\renewcommand{\theequation}{{\sffamily\fontsize{9}{14}\selectfont\thechapter.\arabic{equation}.}} % set equation number font and size

%INCLUDE MACROS
\newcommand{\dedication}{\include{./OtherPages/dedication}\cleardoublepage}
\renewcommand{\thanks}{\include{./OtherPages/thanks}\cleardoublepage}
\newcommand{\fabstract}{\include{./OtherPages/abstract1}\cleardoublepage}
\newcommand{\sabstract}{\include{./OtherPages/abstract2}}
\newcommand{\acronyms}{\include{./OtherPages/acronyms}}
\newcommand{\attachments}{
	\cleardoublepage
	\titleformat{\chapter}
	[display]
	%{\normalfont\sffamily\bfseries\raggedleft}
	%{\sffamily\bfseries\raggedleft}
	{\sffamily\bfseries\filleft}
	{}
	{0pt}
	{\fontsize{16}{6}\selectfont}
	%[<after-code>]
	\titlespacing{\chapter}
	{6cm}
	{64pt}
	{32pt}
	%{37pt}
	\titleformat{\section}
	[hang]
	{\sffamily\bfseries\scshape\filleft}
	{\sffamily\fontsize{11}{11}\selectfont\thesection.}
	{3pt}
	{\fontsize{11}{11}\selectfont}
	\titlespacing{\section}
	{0pt}
	{22pt}
	{0pt}
	\pagenumbering{gobble}
	\pagestyle{plain}
	\include{./OtherPages/attachments}
	%\setcounter{chapter}{1}
	%\renewcommand{\thechapter}{\Alph{chapter}}
}
\newcommand{\attitle}[1]{
	\cleardoublepage
	\chapter*{Anexo \thechapter}
	\section*{#1}
	\newpage
	%\begin{flushright}
	%{\sffamily\bfseries\scshape\fontsize{11}{11}\selectfont#1}
	%\end{flushright}
}
\newcommand{\newchapter}[1]{\include{./Chapters/#1}}
\renewcommand{\title}[1]{\def \thesistitle{#1}} % define title variable
\newcommand{\rvw}[1]{{\color{red}{\textbf{#1}}}}
%\newcommand{\rvw}[1]{#1}
\newcommand{\cit}[1]{{\color{blue}{\textbf{#1}}}}
\newcommand{\newfigure}[5]{
	\begin{figure}[H]
	\centering
	#5
	\includegraphics[#4]{#3}
	%\vspace{-14pt}
	\caption{\sffamily\fontsize{9}{14}\selectfont#1}
	\label{#2}
	\end{figure}
}
\newcommand{\atnewfigure}[2]{
	\begin{figure}[H]
	\centering
	\includegraphics[#2]{#1}
	%\vspace{-14pt}
	\end{figure}
}
\newcommand{\newtable}[3]{
	\begin{table}[H]
	\centering
	\caption{\sffamily\fontsize{9}{14}\selectfont#1}
	\label{#2}
	%\vspace{6pt}
	\sffamily\fontsize{10}{14}\selectfont
	#3
	\end{table}
}
\newcommand{\newequation}[2]{
	\begin{equation}
	\label{#1}
	\centering
	#2
	\end{equation}
}
%\newcommand{\newsnippet}[4]{
%	\lstinputlisting[language=#3,caption={\sffamily\fontsize{9}{14}\selectfont#1},label=#2]{./Snippets/#4}
%}
%input snippet as a figure
\newcommand{\newsnippet}[4]{
	\begin{figure}[H]
	\centering
	\lstinputlisting[language=#3]{./Snippets/#4}
	\caption{\sffamily\fontsize{9}{14}\selectfont#1}
	\label{#2}
	\end{figure}
}
\newcommand{\OK}{\textbf{\textcolor{OliveGreen}{OK}}}
\newcommand{\KO}{\textbf{\textcolor{red}{KO}}}

\setlength{\intextsep}{14pt} % Vertical space above & below [h] floats
\setlength{\abovecaptionskip}{6pt plus 0pt minus 0pt} %set space above caption
\setlength{\belowcaptionskip}{6pt plus 0pt minus 0pt} %set space above caption


%PAGE SETUP
\geometry{
a4paper,
total={160mm,237mm},
left=30mm,
top=30mm,
%headheight=15mm,
headsep=15mm,
footskip=15mm,
marginparwidth=0mm,
marginparsep=0mm
}

\pagenumbering{roman}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{\sffamily\fontsize{8}{8}\selectfont\textit{\thesistitle}}
%\fancyhead[RE,LO]{Guides and tutorials}
%\fancyfoot[CE,CO]{\leftmark}
%\fancyfoot[LE,RO]{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{\scshape\sffamily\fontsize{11}{11}\selectfont Versão para discussão}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\\sffamily\fontsize{10}{10}\selectfont\thepage}
\fancyfoot[LE,RO]{\sffamily\fontsize{10}{10}\selectfont\thepage}

\patchcmd{\chapter}{plain}{fancy}{}{}


%HEADERS COSTUMIZATION
%. _title
\titleformat{\chapter}
[display]
%{\normalfont\sffamily\bfseries\raggedleft}
%{\sffamily\bfseries\raggedleft}
{\sffamily\bfseries\filleft}
{}
{0pt}
{\fontsize{30}{30}\selectfont\thechapter\\\fontsize{16}{6}\selectfont}
%[<after-code>]
\titlespacing{\chapter}
{6cm}
{64pt}
{32pt}
%{37pt}
%. _section
\titleformat{\section}
[hang]
{\sffamily\bfseries\scshape}
{\sffamily\fontsize{11}{11}\selectfont\thesection.}
{3pt}
{\fontsize{11}{11}\selectfont}
\titlespacing{\section}
{0pt}
{22pt}
{0pt}

%. _subsection
\titleformat{\subsection}
[hang]
{\sffamily\scshape}
{\sffamily\fontsize{10}{10}\selectfont\thesubsection.}
{3pt}
{\fontsize{10}{10}\selectfont}
\titlespacing{\subsection}
{0pt}
{21pt}
{0pt}

%. _subsubsection
\titleformat{\subsubsection}
[hang]
{\sffamily}
{\sffamily\fontsize{10}{10}\selectfont\thesubsubsection.}
{3pt}
{\fontsize{10}{10}\selectfont}
\titlespacing{\subsubsection}
{0pt}
{21pt}
{0pt}

%LAYOUTS
\newcommand{\HEADERSINIT}{
\cleardoublepage
\setcounter{page}{1}

%. _tableOfContents
\tocloftpagestyle{fancy}
\renewcommand{\cfttoctitlefont}{\sffamily\fontsize{11}{14}\selectfont\scshape\bfseries} % change TOC title format
\renewcommand{\cftloftitlefont}{\sffamily\fontsize{11}{14}\selectfont\scshape\bfseries} % change LOF title format
\renewcommand{\cftlottitlefont}{\sffamily\fontsize{11}{14}\selectfont\scshape\bfseries} % change LOT title format

\renewcommand\contentsname{Índice Geral} % change TOCs name
\renewcommand\listtablename{Índice de Quadros} % change LOT name
\renewcommand\listfigurename{Índice de Figuras} % change LOF name

\setlength{\cftchapnumwidth}{2.3em} % align chapter title with section title
\renewcommand{\cftchapleader}{\cftdotfill{.8}} % add leader to chapter title
\renewcommand{\cftchapaftersnum}{.} % print an extra dot after chapter number
\renewcommand{\cftsecaftersnum}{.} % print an extra dot after section number
\renewcommand{\cftsubsecaftersnum}{.} % print an extra dot after subsection number
\renewcommand{\cftsubsubsecaftersnum}{.} % print an extra dot after subsubsection number
\renewcommand{\cftdotsep}{.8} % set leader dot step

\setlength{\cftsecindent}{0pt} %remove indent of section
\setlength{\cftsubsecindent}{0pt} %remove indent of subsection
\setlength{\cftsubsubsecindent}{0pt} %remove indent of subsubsection
\renewcommand{\cftchapfont}{\sffamily\fontsize{16}{14}\selectfont\bfseries} %change chapter fonts on TOC
\renewcommand{\cftsecfont}{\sffamily\fontsize{11}{14}\selectfont\bfseries\scshape} %change section fonts on TOC
\renewcommand{\cftsubsecfont}{\sffamily\fontsize{10}{14}\selectfont\scshape} %change subsection fonts on TOC
\renewcommand{\cftsubsubsecfont}{\sffamily\fontsize{10}{14}\selectfont} %change subsubsection fonts on TOC

\renewcommand\cftchappagefont{\sffamily\fontsize{10}{14}\selectfont} %format page number on TOC
\renewcommand\cftsecpagefont{\sffamily\fontsize{10}{14}\selectfont}
\renewcommand\cftsubsecpagefont{\sffamily\fontsize{10}{14}\selectfont}
\renewcommand\cftsubsubsecpagefont{\sffamily\fontsize{10}{14}\selectfont}
\renewcommand\cftfigpagefont{\sffamily\fontsize{10}{14}\selectfont}
\renewcommand\cfttabpagefont{\sffamily\fontsize{10}{14}\selectfont}

\renewcommand{\cftfigpresnum}{Fig. }
\renewcommand{\cfttabpresnum}{Quadro }
\renewcommand{\cftfigaftersnum}{ -}
\renewcommand{\cfttabaftersnum}{ -}
\setlength{\cftfigindent}{0pt}
\setlength{\cfttabindent}{0pt}
\setlength{\cftfignumwidth}{40pt}
\setlength{\cfttabnumwidth}{55pt}
\renewcommand{\cftfigfont}{\sffamily\fontsize{10}{14}\selectfont}
\renewcommand{\cfttabfont}{\sffamily\fontsize{10}{14}\selectfont}
}

\newcommand{\CHAPTERSINIT}{
\cleardoublepage
\setcounter{page}{1}
\pagenumbering{arabic} %reset page numbering
}


%\renewcommand{\chaptertitlename}{}
%\titleformat{\chapter}[frame]
%{\normalfont}
%{\filright
%\footnotesize
%\enspace SECTION \thesection\enspace}
%{8pt}
%{\Large\bfseries\filcenter}
%  {\chaptertitlename \thechapter}{0pt}{\Large}