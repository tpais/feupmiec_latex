.PHONY: build

all: build

build: main.tex
	cd OtherPages; \
	latexmk -pdf fri.tex; \
	latexmk -c; \
	cd ..; \
	latexmk -pdf -pv -f main.tex; \
	latexmk -c