# Template LaTeX para a dissertação de Mestrado em Engenharia Civil - FEUP

Pré-Requisitos
==============
## Instalar distribuição LaTeX
**Windows:**
https://miktex.org/

**MacOS:**
http://www.tug.org/mactex/


## Instalar `make` para correr build automática do pdf
**Windows:**
http://gnuwin32.sourceforge.net/packages/make.htm


**MacOS:**

Abrir Terminal: (cmd + space) terminal
```sh
xcode-select --install
```

Como utilizar o template
========================
## Criação dos capítulos
Os capítulos encontram-se na pasta `Chapters`. Para adicionar capítulos, duplicar um dos ficheiros existentes e alterar o nome para `Chapter<num>.tex`, em seguida abrir o ficheiro `main.tex` e criar entrada para o novo capítulo junto dos capítulos já existentes.
Exemplo:

```latex
\CHAPTERSINIT
\newchapter{Chapter1}
\newchapter{Chapter2}
\newchapter{Chapter3}
```

## Figuras, tabelas e equações
Nos dois capítulos de exemplo distriubuidos com o template existem exemplos de figuras, tabelas e equações. Para adicionar qualquer um destes elementos copiar o código de exemplo e modificar de acordo com o pretendido.

Para referenciar qualquer um destes elementos no texto utilizar o comando `\ref{<label do elemento>}`.

## Bibliografia
Para adicionar bibliografia, adicionar entradas `bibtex` ao ficheiro `references.bib`. Para citar as entradas no texto basta adicionar o comando `\cite{<label da entrada>}` no local pretendido.

## Restantes secções do documento, Capa, Resumo, Agradecimentos, etc
Estas páginas encontram-se na pasta `OtherPages`. A capa encontra-se no ficheiro`fri.tex`, os agradecimentos no ficheiro `thanks.tex` e os anexos no ficheiro `attachments.tex`. A contra-capa e a dedicatória não têm atualmente um ficheiro `.tex` dedicado, em vez disso os ficheiros são exportados para `pdf` atravéz do `msword` e guardados nesta pasta com os nomes `vfri_temp.pdf` e `dedication.pdf` respectivamente.

Gerar output em PDF
===================
Para gerar o output em pdf, ficheiro que terá o nome `main.pdf`, executar o comando na `linha de comandos` do Windows ou no `terminal` do Mac:

```shell
make build
```

A partir da pasta raiz do repositório, onde se encontra o ficheiro `makefile`.

Notas
=====
O template encontra-se preenchido com texto gerado aleatóriamente para exemplificação da resultado da compilação. Todos os comandos `\lipsum` encontrados nos ficheiros `.tex` são responsáveis pela geração deste texto. Apagar ou comentar estes comandos à medida que for preenchendo os templates com o texto pretendido.